//
// Created by Osvaldo Moreno Ornelas on 5/12/16.
//


#include "Huffman.h"
#include <fstream>
//
#include <typeinfo>

Huffman::Huffman() { }

Huffman::~Huffman() { }

BinaryTree * Huffman::getFront()
{
	BinaryTree * retTree;

	//check if B is empty or A has elements, and the frequency of A is smaller than the front of B
	if(B.size() == 0 || (A.size() != 0 && A.front()->getValue() < B.front()->getValue()))
	{
		//use the front of A to add to T3
		retTree  = A.front();
		A.pop_front();
		return retTree;
	}

	//This returns the element of B if B front  is the smaller of A and B 
	retTree = B.front();
	B.pop_front();

	return retTree;
}


void Huffman::createTable(string convertStr){

//for every char iin the set, increase an instace using the ascii value
	for(char c : convertStr)
	{
		//creates set of unique character values
		charSet.insert (c);

		//create a parallel array of frequencies
		frequencies[int(c)]++;
	}
}

void Huffman::createHeap(){

	BinaryTree * rootTree;


	for(char c : charSet){

		rootTree = new BinaryTree(c,frequencies[int(c)]);
		//push into the heap for the heap sort
		codeHeap.Insert(rootTree);
	}
	
	/*
	sort(A.begin(), A.end(), [ ](const BinaryTree*& lhs, const BinaryTree*& rhs)
	{
		return lhs->getValue() < rhs->getValue();
	} );*/
	
	//pop the min from the heap to have a sorted array (heap sort)
	int n = codeHeap.Size();
	for(int i=0; i < n; i++)
	{
		A.push_back(codeHeap.MinElement());
		codeHeap.removeMin();
	}

}

void Huffman::outputFrequenceAndChar()
{

	//initialize the frequencies from char set
	//char set allows to have unique characters in the table.
	for(char c : charSet)
	{
		cout << endl << c << endl;
		cout << frequencies[int(c)] << endl;
	}

}

//make the binary tree by following the algorithm in the Data Structures
//book.
BinaryTree * Huffman::createTree(){


	//right
	BinaryTree *tree1;

	//left
	BinaryTree *tree2;

	//root
	BinaryTree *tree3;


	
	int n = A.size();

//iterate through all elements of A
for(int itr = 1; itr < n; itr++)
{
	
	//min
	//on first iteration t1 and t2 come from A
	tree1 = getFront();
	
	//second min
	tree2 = getFront();

	tree3 = new BinaryTree();

	//set the value k
	tree3->setValue (tree1->getValue()+tree2->getValue ());

	//set the new left and right
	tree3->setLeft (tree1->getRoot());

	tree3->setRight (tree2->getRoot());

	//insert new tree into B, the one that keeps track of the current 
	//tree and newly created keys
	B.push_back(tree3);
}


BinaryTree *returnTree;

//At this point B has only one element, and is the built tree
returnTree = B.front();

//return tree with the codes
return returnTree;

}

//this encodes the characters to a huffman code
void Huffman::encode(BinaryTree* codeTree,string decodeStr, string fileName){

	//void char that will get chat with code
string str  = "";

ofstream oFile;
oFile.open(fileName.c_str());

ostringstream out;


//for very character in the string
//for(char c : decodeStr)
for(int i = 0; i < decodeStr.size(); ++i)
{

	//get the code for that character
	codeTree->huf(codeTree->getRoot(), decodeStr[i], "", str );

	//save codes
	out << str.c_str();
}


cout << endl << "Huffman coded: " << endl;
cout << out.str().length();

oFile<< out.str();

oFile.close();

}

//decode the huffman code
string Huffman::decode(BinaryTree * myTree, string FileName){

	ostringstream out;
	stringstream outCode;


	//open file with the codes
	ifstream inFile;

	cout << endl << FileName << endl;

	inFile.open(FileName.c_str());

	string decodedStr;

	//get codes from file
	inFile >> decodedStr;

	string showStr = "";


	//get the root of the huffman tree
	Node * myNode = myTree->getRoot();
	ofstream outFile;
	outFile.open("AddressCodes.txt");

	//for every character in the coded string
	for(int i = 0; i < decodedStr.size(); i++)
	{

		char c = decodedStr[i];

		showStr += c;

		outFile << showStr;

		//cout << showStr;

		//if is the bottom of the tree
		if(myNode->leftLink == NULL && myNode->rightLink == NULL)
		{

			//put that character in the return string
			out << myNode->info;
			myNode = myTree->getRoot();
		}

		//if the value is 0
		if(c == '0')
		{

			//go left
			myNode = myNode->leftLink;
		}
		else if(c == '1')
		{
			//if 1, go right
			myNode = myNode->rightLink;
		}
	}//end of the for loop

	out << myNode->info;




	//outFile << outCode.str();

	outFile.close();
	inFile.close();

	return out.str();

}

//otput the sequences
string Huffman::outputTable(BinaryTree* codeTree, int &compressedRatio)
{

	ostringstream out;
//int compressedRatio;

	string str = "";

	out << left;
	out << setw(15)<< "Character"  << setw(15) << "Frequence" << "Huffman Code" << endl;
	out << "-------------------------------------------" << endl;


	for(char c : charSet)
	{
		codeTree->huf(codeTree->getRoot(), c, "", str );
		compressedRatio += (str.length()*frequencies[int(c)]);


		out << c << " has frequence of " << setw(5) << frequencies[int(c)] << " with the huffman code:  " << str << endl;

	}

	return out.str();
}
