CC = g++
CXXFLAGS = -O3 -mcx16 -march=native -std=c++17 -Wall -Wextra -fcilkplus -DCILK -fpic

all: huff

huff: main.cpp Huffman.h Huffman.cpp Heap.h Heap.cpp Functions.h BTree.h BinaryTree.cpp get_time.h
	$(CC) $(CXXFLAGS) main.cpp Huffman.h Huffman.cpp Heap.h Heap.cpp Functions.h BTree.h BinaryTree.cpp get_time.h -o huffman

clean:
	rm huffman

.PHONY: clean
