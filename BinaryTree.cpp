/*
 * BinaryTree.cpp
 *
 *  Created on: May 13, 2016
 *      Author: omorenoornelas0
 */
#include "BTree.h"
#define width_unit 5


BinaryTree::BinaryTree()
{
    root = new Node();
}

BinaryTree::BinaryTree(char newInfo, int newVal)
{
	root = new Node();
    root->info = newInfo;
    root->value = newVal;

}


BinaryTree::BinaryTree (const BinaryTree& otherTree)
{
	if (otherTree.root == NULL) //otherTree is empty
	root = NULL;
	else
	copyTree(root, otherTree.root);
}

void BinaryTree::copyTree
(Node* &copiedTreeRoot,
Node* otherTreeRoot)
{
if (otherTreeRoot == NULL)
copiedTreeRoot = NULL;
else
{
copiedTreeRoot = new Node();
copiedTreeRoot->info = otherTreeRoot->info;
copyTree(copiedTreeRoot->leftLink, otherTreeRoot->leftLink);
copyTree(copiedTreeRoot->rightLink, otherTreeRoot->rightLink);
}
} //en



BinaryTree:: ~BinaryTree(){}


void BinaryTree:: huf(Node * myNode, unsigned char c, string str, string &str2) const
{
	if(myNode != NULL)
	{
		if(!myNode->leftLink && !myNode->rightLink && myNode->info == c){
			str2 = str;
		}
		else
		{
			huf(myNode->leftLink,c,str+"0",str2);
			huf(myNode->rightLink,c,str+"1",str2);
		}
	}
}

void BinaryTree:: huffmanList(Node * myNode, string str) const
{
	if(myNode != NULL)
	{
		if(!myNode->leftLink && !myNode->rightLink)
		{
			cout << print_char(myNode);
		}
		else
		{
			huffmanList(myNode->leftLink, str+"0");
			huffmanList(myNode->rightLink, str+"1");
		}
	}
}


bool BinaryTree:: IsEmpty()
{
    return root == NULL;
}

char BinaryTree:: getInfo() const
{
    return root->info;
}

int BinaryTree:: getValue() const
{
return root->value;
}


void BinaryTree:: setInfo(char newInfo){

    root->info = newInfo;

}

void BinaryTree:: setValue(int newValue){


    root->value = newValue;

}


void BinaryTree:: setLeft(Node* newLeft){

    root->leftLink = newLeft;

}
void BinaryTree:: setRight(Node* newRight){
    root->rightLink = newRight;
}

Node * BinaryTree:: getRoot(){
    return root;
}

void BinaryTree:: InOrderTraversal()
{
    inorder(root);
}


void BinaryTree :: PreOrderTraversal()
{
    preorder(root);
}

void BinaryTree::  PostOrderTraversal()
{
    preorder(root);
}

int BinaryTree:: TreeHeight()
{
    return height(root);
}

int BinaryTree:: ThreeNodeCount()
{
    return nodecount(root);
}

int BinaryTree:: ThreeLeavesCount()
{
    return leavesCount(root);
}

void BinaryTree ::  DestroyTree()
{
    destroy(root);
}


void BinaryTree::  inorder(Node*p)
{
    if(p!=NULL)
    {
        inorder(p->leftLink);
        cout << p->info << " " << p->value;
        inorder(p->rightLink);
    }

}


void BinaryTree::  preorder(Node *p)
{
    if(p!=NULL)
    {
        cout << p->info << " ";
        preorder(p->leftLink);
        preorder(p->rightLink);

    }

}

void BinaryTree::  postorder(Node *p)
{
    if(p!=NULL)
    {
        postorder(p->leftLink);
        postorder(p->rightLink);
        cout << p->info << " ";

    }

}

int BinaryTree:: height(Node *p)
{
    if(p==NULL)
    {
        return 0;
    }
    else
    {
        return 1+max(height(p->leftLink),height(p->rightLink));
    }

}


int BinaryTree:: max(int x, int y)
{
    if(x>=y)
    {
        return x;
    }
    else
    {
        return y;
    }

}


int BinaryTree :: nodecount(Node*p)
{
    int nodeCounter = 0;
    if(p!=NULL)
    {
        if(p->leftLink != NULL || p->rightLink!= NULL)
        {
            nodeCounter++;
        }

    }
    return nodeCounter;
}

int BinaryTree :: leavesCount(Node *p)
{
    int leafCounter = 0;
    if(p!=NULL)
    {
        if(p->rightLink == NULL && p->leftLink==NULL)
        {

            leafCounter++;
        }

    }

    return leafCounter;
}

void BinaryTree :: destroy(Node* &p)
{
    if(p!=NULL)
    {
        destroy(p->leftLink);
        destroy(p->rightLink);
        delete p;
        p = NULL;
    }

}

string BinaryTree :: Print(printType method)
{
    ostringstream out;

    switch(method)
    {
    case IN_ORDER: InOrderTraversal();
    	break;
    case PRE_ORDER:  PreOrderTraversal();
    	break;
    case POST_ORDER: PostOrderTraversal();
    	break;
    }
    return out.str();

}

//the recursive tree output (w/ respect to its graph) fn.
void BinaryTree::print(ostream & ost, Node * curr, int level) const
{
    if(curr) //if the current node is not null then
    {
        print(ost,curr->rightLink,level+1); //try to go to right node
        //output the node data w/ respect to its level
        ost<<setw(level*width_unit)<<print_char(curr)<<":"
           <<curr->value<<endl;
        print(ost,curr->leftLink,level+1); //try to go to left node
    }
}

//the recursive tree print (w/ respect to its graph) fn.
void BinaryTree::print(Node * curr, int level) const
{
    if(curr) //if the current node is not null then
    {
        print(curr->rightLink,level+1); //try to go to right node
        //print the node data w/ respect to its level
        cout<<setw(level*width_unit)<<print_char(curr)<<":"
            <<curr->value<<endl;
        print(curr->leftLink,level+1); //try to go to left node
    }
}

string BinaryTree::print_char(Node * N) const
{
    string s="";

    if(!N->leftLink && !N->rightLink) //if it is a leaf node
    {
        unsigned char c=N->info;

        //if the char is not printable then output its octal ASCII code
        if(iscntrl(c) || c==32) //32:blank char
        {
            //calculate the octal code of the char (3 digits)
            char* cp=new char;
            for(int i=0;i<3;++i)
            {
                sprintf(cp,"%i",c%8);
                c-=c%8;
                c/=8;
                s=(*cp)+s;
            }
            s='/'+s; // adds \ in front of the octal code
        }
        else
            s=c;
    }
    return s;
}

// Find the maximum height of the binary tree
int maxHeight(Node *p) {
  if (!p) return 0;
  int leftHeight = maxHeight(p->leftLink);
  int rightHeight = maxHeight(p->rightLink);
  return (leftHeight > rightHeight) ? leftHeight + 1: rightHeight + 1;
}

// Convert an integer value to string
string intToString(int val, char info) {
  ostringstream ss;
  ss << val;
  //ss << ',';
  ss << info;
  return ss.str();
}
